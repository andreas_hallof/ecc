/*
       https://bugzilla.redhat.com/show_bug.cgi?id=1746879
*/

import java.security.Security;
import java.util.*;

public class auflisten
{
  public static void main(String[] args)
    throws Exception
  {
    String[] curves = Security.getProvider("SunEC").getProperty("AlgorithmParameters.EC SupportedCurves").split("\\|");
    System.out.println(Arrays.toString(curves));
  }
}

