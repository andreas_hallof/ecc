import java.security.*;
import java.security.spec.*;
import java.util.*;


public class myecc {

      public static void main(String[] args) throws Exception {
            KeyPairGenerator kpg;

            kpg = KeyPairGenerator.getInstance("EC", "SunEC");
            ECGenParameterSpec ecsp;
            ecsp = new ECGenParameterSpec("brainpoolP256r1");
            // ecsp = new ECGenParameterSpec("secp256r1");

            try {
                  kpg.initialize(ecsp);

            } catch (InvalidAlgorithmParameterException e) {

                  System.out.println(e);

            }

            KeyPair kp = kpg.genKeyPair();
            PrivateKey privKey = kp.getPrivate();
            PublicKey pubKey = kp.getPublic();
            System.out.println("priv: " + privKey);
            System.out.println("pub:  " + pubKey);
      }

}
