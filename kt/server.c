/* Starting-point: https://wiki.openssl.org/index.php/Simple_TLS_Server
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <string.h>

int create_socket(int port)
{
    int s;
    struct sockaddr_in addr;

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
	perror("Unable to create socket");
	exit(EXIT_FAILURE);
    }

    if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
	perror("Unable to bind");
	exit(EXIT_FAILURE);
    }

    if (listen(s, 1) < 0) {
	perror("Unable to listen");
	exit(EXIT_FAILURE);
    }

    return s;
}

void init_openssl()
{ 
    SSL_load_error_strings();	
    OpenSSL_add_ssl_algorithms();
}

void cleanup_openssl()
{
    EVP_cleanup();
}

SSL_CTX *create_context()
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    printf("Erlaube nur TSL Version 1.2\n");
    // method = SSLv23_server_method();
    method = TLSv1_2_method();

    ctx = SSL_CTX_new(method);
    if (!ctx) {
	perror("Unable to create SSL context");
	ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    return ctx;
}

void configure_context(SSL_CTX *ctx)
{
    const char MyCipherList[] = "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384";

    SSL_CTX_set_ecdh_auto(ctx, 1);
    /* Set the RSA-key and RSA-cert */
    printf("Loading RSA Key + Cert.\n");
    if (SSL_CTX_use_certificate_file(ctx, "kt_cert_rsa.pem", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, "kt_key_rsa.pem", SSL_FILETYPE_PEM) <= 0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    if (SSL_CTX_check_private_key(ctx) <= 0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    /* Set the ECC-key and ECC-cert */
    printf("Loading ECC Key + Cert.\n");
    if (SSL_CTX_use_certificate_file(ctx, "kt_cert_ecc.pem", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, "kt_key_ecc.pem", SSL_FILETYPE_PEM) <= 0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    if (SSL_CTX_check_private_key(ctx) <= 0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    printf("Ich fixiere die Liste der akzeptierten Ciphersuiten auf:\n%s\n", 
           MyCipherList);
    if (SSL_CTX_set_cipher_list(ctx, MyCipherList) <=0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv)
{
    int sock;
    SSL_CTX *ctx;
    const char * ciphername;
    const SSL_CIPHER * current_cipher;
    const int myport=4433;

    init_openssl();
    ctx = create_context();

    configure_context(ctx);

    sock = create_socket(myport);

    printf("Server ist gestartet und wartet auf Verbindungsanfragen auf Port %d.\n", myport);
    /* Handle connections */
    while(1) {
        struct sockaddr_in addr;
        uint len = sizeof(addr);
        SSL *ssl;
#define MAXSTR 200
        const char reply1[] = "Server: Hallo, ich arbeitet unter ",
                   reply2[] = ".\n",
                   format1[] = "Wenn jetzt ein initiales Pairing wäre, würde ich als Signaturalgorithmus %s verwenden.\n";
        char       reply3[MAXSTR], str_signaturalgorithmus[MAXSTR];

        int client = accept(sock, (struct sockaddr*)&addr, &len);
        if (client < 0) {
            perror("Unable to accept");
            exit(EXIT_FAILURE);
        }

        ssl = SSL_new(ctx);
        SSL_set_fd(ssl, client);

        if (SSL_accept(ssl) <= 0) {
            ERR_print_errors_fp(stderr);
        }
        else {
            printf("Neue Verbindung: OK, los geht es.\n");
            // printf("get_cipher(): %s\n", SSL_get_cipher(ssl));
            current_cipher=SSL_get_current_cipher(ssl);
            ciphername=SSL_CIPHER_get_name(current_cipher);
            printf("aktuell verwendete Ciphersuite: %s\n", ciphername);
            if (strncmp("ECDHE-RSA", ciphername, 9)==0) {
                snprintf(str_signaturalgorithmus, MAXSTR, "RSA");
            } else {
                snprintf(str_signaturalgorithmus, MAXSTR, "ECDSA");
            }

            snprintf(reply3, MAXSTR, format1, str_signaturalgorithmus);
            printf(reply3);

            SSL_write(ssl, reply1, strlen(reply1));
            SSL_write(ssl, ciphername, strlen(ciphername));
            SSL_write(ssl, reply2, strlen(reply2));
            SSL_write(ssl, reply3, strlen(reply3));
        }

        SSL_free(ssl);
        close(client);
    }

    close(sock);
    SSL_CTX_free(ctx);
    cleanup_openssl();
}

