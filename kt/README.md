# RSA und ECC basierte Ciphersuiten parallel unterstützen

# openssl / libressl

Ich starte den Server

    $ ./server
    Erlaube nur TSL Version 1.2
    Loading RSA Key + Cert.
    Loading ECC Key + Cert.
    Ich fixiere die Liste der akzeptierten Ciphersuiten auf:
    ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384
    Server ist gestartet und wartet auf Verbindungsanfragen auf Port 4433.

und erzeuge eine Verbindung

    $ openssl s_client -connect 127.0.0.1:4433
    CONNECTED(00000005)
    depth=0 C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
    verify error:num=18:self signed certificate
    verify return:1
    depth=0 C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
    verify return:1
    ---
    Certificate chain
     0 s:C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
       i:C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
    ---
    Server certificate
    -----BEGIN CERTIFICATE-----
    MIICHzCCAcWgAwIBAgIUQO7WnxgiAVEanoCs6/fIqzhF8iUwCgYIKoZIzj0EAwIw
    ZTELMAkGA1UEBhMCREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGlu
    MRAwDgYDVQQKDAdnZW1hdGlrMRAwDgYDVQQLDAdnZW1hdGlrMRAwDgYDVQQDDAdL
    VC0xMjM0MB4XDTE4MTEwNTIyMDE0MVoXDTE5MTEwNTIyMDE0MVowZTELMAkGA1UE
    BhMCREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGluMRAwDgYDVQQK
    DAdnZW1hdGlrMRAwDgYDVQQLDAdnZW1hdGlrMRAwDgYDVQQDDAdLVC0xMjM0MFkw
    EwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEObNebPBCT/9xh3wY1cFkaWlD+x4PA+vx
    BkWwB71qss5Y3xby7q3pbWa2G9awrSPbwvh8agRb5CBg6R/BUouQAqNTMFEwHQYD
    VR0OBBYEFBqmZDwaAIZ3ETsI4jafK7Krtp59MB8GA1UdIwQYMBaAFBqmZDwaAIZ3
    ETsI4jafK7Krtp59MA8GA1UdEwEB/wQFMAMBAf8wCgYIKoZIzj0EAwIDSAAwRQIh
    AJnGzOr4dMRm/0cH77PEAaDZLxLnRdowlnReW7E7ayPSAiBRMwUDPbOKBWPUnwOF
    Xrc7LyOlZ2tSqAFak7dIxs5pRQ==
    -----END CERTIFICATE-----
    subject=C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234

    issuer=C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234

    ---
    No client certificate CA names sent
    Peer signing digest: SHA256
    Peer signature type: ECDSA
    Server Temp Key: X25519, 253 bits
    ---
    SSL handshake has read 986 bytes and written 404 bytes
    Verification error: self signed certificate
    ---
    New, TLSv1.2, Cipher is ECDHE-ECDSA-AES256-GCM-SHA384
    Server public key is 256 bit
    Secure Renegotiation IS supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    SSL-Session:
        Protocol  : TLSv1.2
        Cipher    : ECDHE-ECDSA-AES256-GCM-SHA384
        Session-ID: 1B0D8CFA4FD533C441EEE0E54F8A23D47BFC944E8A655E917EEABFDB6A095D4A
        Session-ID-ctx:
        Master-Key: 431550987FD3F092CA0C36043D2830B3E114DFD744728A0DE57251E24CEF695BD5B6B245F46CC42299617B314C373496
        PSK identity: None
        PSK identity hint: None
        SRP username: None
        TLS session ticket lifetime hint: 7200 (seconds)
        TLS session ticket:
        0000 - 7e 51 85 36 04 04 1a 9d-de 90 dd d4 e1 db a5 02   ~Q.6............
        0010 - 8b 38 90 af 37 ab e0 7e-17 ea ba ef f3 53 8d 34   .8..7..~.....S.4
        0020 - 7b f2 ba 0c fd 6c ff 2f-c9 06 0d 1b 8f 7a 25 1d   {....l./.....z%.
        0030 - f2 92 57 af ea c8 61 de-31 42 a1 62 67 af bd 74   ..W...a.1B.bg..t
        0040 - da 50 42 f5 32 17 6d 2a-72 d3 5b 67 d0 14 d9 a9   .PB.2.m*r.[g....
        0050 - b5 b3 de fc 00 b6 4c 04-36 13 99 0e 6e d5 1e 0f   ......L.6...n...
        0060 - e3 a5 d3 94 ac b0 bd cc-d5 ae d1 f2 2b 1e 0d 51   ............+..Q
        0070 - 37 ee 4a bc 04 44 67 c2-c1 e4 11 3f db e3 d4 44   7.J..Dg....?...D
        0080 - d1 da 14 e0 a7 f8 6b de-39 6a 48 af fe cf 0d 7c   ......k.9jH....|
        0090 - 0c c1 30 ed cc 6f aa 10-b2 95 f7 b2 80 0b e2 4d   ..0..o.........M

        Start Time: 1541505934
        Timeout   : 7200 (sec)
        Verify return code: 18 (self signed certificate)
        Extended master secret: yes
    ---
    Server: Hallo, ich arbeitet unter ECDHE-ECDSA-AES256-GCM-SHA384.
    Wenn jetzt ein initiales Pairing wäre, würde ich als Signaturalgorithmus ECDSA verwenden.
    read:errno=0

Beim Server erscheint auf der Standard-Ausgabe:

    Neue Verbindung: OK, los geht es.
    aktuell verwendete Ciphersuite: ECDHE-ECDSA-AES256-GCM-SHA384
    Wenn jetzt ein initiales Pairing wäre, würde ich als Signaturalgorithmus ECDSA verwenden.

Ich erzwinge mal die RSA-Verwendung:

    $ openssl s_client -connect 127.0.0.1:4433 -cipher ECDHE-RSA-AES256-GCM-SHA384
    CONNECTED(00000005)
    depth=0 C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
    verify error:num=18:self signed certificate
    verify return:1
    depth=0 C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
    verify return:1
    ---
    Certificate chain
     0 s:C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
       i:C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234
    ---
    Server certificate
    -----BEGIN CERTIFICATE-----
    MIIDnTCCAoWgAwIBAgIJAKhDNfbn1wiQMA0GCSqGSIb3DQEBCwUAMGUxCzAJBgNV
    BAYTAkRFMQ8wDQYDVQQIDAZCZXJsaW4xDzANBgNVBAcMBkJlcmxpbjEQMA4GA1UE
    CgwHZ2VtYXRpazEQMA4GA1UECwwHZ2VtYXRpazEQMA4GA1UEAwwHS1QtMTIzNDAe
    Fw0xODExMDQxOTUwNTJaFw0xOTExMDQxOTUwNTJaMGUxCzAJBgNVBAYTAkRFMQ8w
    DQYDVQQIDAZCZXJsaW4xDzANBgNVBAcMBkJlcmxpbjEQMA4GA1UECgwHZ2VtYXRp
    azEQMA4GA1UECwwHZ2VtYXRpazEQMA4GA1UEAwwHS1QtMTIzNDCCASIwDQYJKoZI
    hvcNAQEBBQADggEPADCCAQoCggEBANnOAmU+5GPwW/UoEBZ09KXJTDVVNcIHhFYh
    n7Hlr03NKISiurf4yLP8yXrQmpp0KnSIjjk8+LRizWhZP+Pe8e2pyv6+KdzwaikG
    eGrQEZ/O8VuVim3AIrZkfGQyIlGmn7zA4mw2VsxCjiOxsm50mjjhX1IkTtuBDk+7
    YcDG/mkvMScRA28KwzPbGS7cZ2UkB5LyMfvhRX4oksYTDCRGSHPy9L80W+eZUJvD
    j6O84zmKVnUgGKDz71pgRPhUb16F/4XwUpg54BRtbUgiVFzffOf9Mb+GDuFWSjrP
    F0BOEKrbKM+NGO2BRwhXuquzBJb8BYP4o5qvfqRmBdRMIH6RDysCAwEAAaNQME4w
    HQYDVR0OBBYEFJXZmqlIedrRWIMXvdFR5kAWytG4MB8GA1UdIwQYMBaAFJXZmqlI
    edrRWIMXvdFR5kAWytG4MAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEB
    AI78bHPx6lGuatcGIO1jZ1DbrdFVsEc7aQCSub3XjLOfMpbqWgAhMD67wd2B3pBj
    GuPclnk/Dp8va7iYLfhurMS0YsznC55/eUbjRmojl2qep3vL3vxmjSAtU4IHthbH
    7v0r5Bz6WZP2PoWm4uzvHsCD9hCjjbfeY+KQoCLaR7hbtfglTF6QF8Oujqn/Knsv
    P1gHtgACpZEJ555LE7+MWRd03iGj8RwyWL8HiMZBG2sL7dZs/j9hkkQ8HK8j+X/B
    GIL6KKqJ+IMab79AnDbC4yor437zC5IJsD5Gjf8g3P0SnTuXe0XVd+jv9qReIVs1
    TefsUCIlcmKLnvPzUY66YYE=
    -----END CERTIFICATE-----
    subject=C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234

    issuer=C = DE, ST = Berlin, L = Berlin, O = gematik, OU = gematik, CN = KT-1234

    ---
    No client certificate CA names sent
    Peer signing digest: SHA256
    Peer signature type: RSA-PSS
    Server Temp Key: X25519, 253 bits
    ---
    SSL handshake has read 1554 bytes and written 352 bytes
    Verification error: self signed certificate
    ---
    New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
    Server public key is 2048 bit
    Secure Renegotiation IS supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    SSL-Session:
        Protocol  : TLSv1.2
        Cipher    : ECDHE-RSA-AES256-GCM-SHA384
        Session-ID: FDF4B873FDC36605B1148EE78690B8C28408230714B57C67D5468AC4F0A9F261
        Session-ID-ctx:
        Master-Key: 36FAAD21BD5AA3AEAB69D4199109A32D3B6A5C96A09F9CFBFEAF4CE66D1C4CF91EFC2FE230F3650F3005E8B3B45E0ABC
        PSK identity: None
        PSK identity hint: None
        SRP username: None
        TLS session ticket lifetime hint: 7200 (seconds)
        TLS session ticket:
        0000 - 7e 51 85 36 04 04 1a 9d-de 90 dd d4 e1 db a5 02   ~Q.6............
        0010 - ba eb 46 b4 b3 d0 a6 47-b6 1b a6 b4 a4 93 80 9d   ..F....G........
        0020 - 7f a9 d0 e9 89 38 c6 b3-3f e4 01 e0 c9 50 8d 08   .....8..?....P..
        0030 - dd 2f 6a 3f 58 be c1 ad-35 ff 57 e1 30 f7 f8 19   ./j?X...5.W.0...
        0040 - 6d 99 82 3e 32 72 74 53-1c 37 bd 6d c0 eb 99 9d   m..>2rtS.7.m....
        0050 - e5 7d c9 c6 bc 2f ab 81-6d 1e 27 73 e4 36 aa e1   .}.../..m.'s.6..
        0060 - 31 dd 66 25 00 47 81 64-77 b2 90 cb 74 83 20 d7   1.f%.G.dw...t. .
        0070 - 47 3f 87 f9 5a 65 30 5e-c1 49 09 a5 58 17 17 69   G?..Ze0^.I..X..i
        0080 - 71 bb c3 b0 93 80 21 4e-f1 97 6e 5c 35 6c c3 df   q.....!N..n\5l..
        0090 - 7e 62 a0 3d c7 0b f0 05-b0 94 bc 76 99 9a 29 56   ~b.=.......v..)V

        Start Time: 1541506178
        Timeout   : 7200 (sec)
        Verify return code: 18 (self signed certificate)
        Extended master secret: yes
    ---
    Server: Hallo, ich arbeitet unter ECDHE-RSA-AES256-GCM-SHA384.
    Wenn jetzt ein initiales Pairing wäre, würde ich als Signaturalgorithmus RSA verwenden.
    read:errno=0



    $ sudo nmap -sV --script ssl-enum-ciphers -p 4433 127.0.0.1
    [sudo] Passwort für a: 
    Starting Nmap 7.70 ( https://nmap.org ) at 2018-11-05 23:07 CET
    Nmap scan report for localhost.localdomain (127.0.0.1)
    Host is up (0.000044s latency).

    PORT     STATE SERVICE  VERSION
    4433/tcp open  ssl/vop?
    | fingerprint-strings: 
    |   NULL: 
    |_    test
    | ssl-enum-ciphers: 
    |   TLSv1.2: 
    |     ciphers: 
    |       TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384 (secp256r1) - A
    |       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (secp256r1) - A
    |     compressors: 
    |       NULL
    |     cipher preference: client
    |_  least strength: A
    1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
    SF-Port4433-TCP:V=7.70%T=SSL%I=7%D=11/5%Time=5BE0BF0B%P=x86_64-unknown-lin
    SF:ux-gnu%r(NULL,5,"test\n");

    Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
    Nmap done: 1 IP address (1 host up) scanned in 7.72 seconds

# merker signatur
https://wiki.openssl.org/index.php/EVP_Signing_and_Verifying


# merker mal in python schreiben
http://www.rosettacode.org/wiki/Safe_primes_and_unsafe_primes

# mbedtls
https://tls.mbed.org/discussions/crypto-and-ssl/how-to-use-ecc-and-rsa-certificates-simultaneously

