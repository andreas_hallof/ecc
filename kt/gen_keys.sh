#! /usr/bin/env bash

#openssl ecparam -name brainpoolP256r1 -genkey -out kt_key_ecc.pem
openssl ecparam -name prime256v1 -genkey -out kt_key_ecc.pem
openssl req -x509 -key kt_key_ecc.pem \
-out kt_cert_ecc.pem -days 365 \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=KT-1234"


openssl genrsa -out kt_key_rsa.pem 2048
openssl req -x509 -key kt_key_rsa.pem -out kt_cert_rsa.pem -days 365 \
    -subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=KT-1234"

