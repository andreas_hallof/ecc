/*
    https://makeinjava.com/configure-jca-jce-provider-execution-order-java-security/
*/

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
 
public class ProviderOrderExecution {
 
    public static void main(String[] args) throws NoSuchPaddingException,
            NoSuchAlgorithmException, NoSuchProviderException {
 
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
 
        String algorithm = cipher.getAlgorithm();
        Provider provider = cipher.getProvider();
        int blockSize = cipher.getBlockSize();
 
        System.out.println("Output using default Provider:");
        System.out.println("Algorithm :"+algorithm);
        System.out.println("Provider Name:"+provider.getName());
        System.out.println("Block Size :"+blockSize);
 
        System.out.println("\nOutput using Bouncy Castle (BC) Provider:");
        cipher = Cipher.getInstance("AES","BC");
        algorithm = cipher.getAlgorithm();
        provider = cipher.getProvider();
        blockSize = cipher.getBlockSize();
 
        System.out.println("Algorithm :"+algorithm);
        System.out.println("Provider Name:"+provider.getName());
        System.out.println("Block Size :"+blockSize);
    }
}

