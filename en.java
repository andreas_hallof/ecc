
import java.util.*;
import java.security.*;
import java.security.spec.*;


// import sun.security.ec.CurveDB.*;

public class en {

    public static void main(String[] args) throws Exception {

        System.out.println("Hello World");

        KeyPairGenerator kpg;
        kpg = KeyPairGenerator.getInstance("EC","SunEC");
        ECGenParameterSpec ecsp;
        ecsp = new ECGenParameterSpec("secp256r1");
        // ecsp = new ECGenParameterSpec("secp256k1");
        kpg.initialize(ecsp);

        KeyPair kp = kpg.genKeyPair();
        PrivateKey privKey = kp.getPrivate();
        PublicKey pubKey = kp.getPublic();

        System.out.println(privKey.toString());
        System.out.println(pubKey.toString());
    }
        /*
        public static void main(String[] args) throws Exception {

            System.out.println("Hello World");
            Method method = sun.security.ec.CurveDB.class.getDeclaredMethod("getSupportedCurves", null);
            method.setAccessible(true);
            Collection result = (Collection) method.invoke(null, null);

            for (Object object : result) {
                System.out.println(object);
            }
        }
        */
}

