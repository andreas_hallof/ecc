/*
       https://bugzilla.redhat.com/show_bug.cgi?id=1746879
*/

import java.security.KeyPair;
import java.security.spec.ECGenParameterSpec;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

public class ecckp
{
  public static void main(String[] args)     throws Exception {
     KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC");
     ECGenParameterSpec ecs = new ECGenParameterSpec("secp256r1");
     keyGen.initialize(ecs, new SecureRandom());
     KeyPair pair = keyGen.genKeyPair();
     PrivateKey priv = pair.getPrivate();
     PublicKey pub = pair.getPublic();
     System.out.println(pub.toString());
     //System.out.println(priv.toString());
  }
}

